<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title> Teste de programa&ccedil;&atilde;o - @yield('title')</title>
        <meta charset="utf-8">
        <meta name="keywords" content="teste, programacao, leroy-merlin">
        <meta name="description" content="Teste realizado para dev php">
        <meta name="author" content="Evandro Ishy Medeiros">
        <link href="{!! asset('assets/css/bootstrap.min.css') !!}" media="all" rel="stylesheet" type="text/css" />
        <link href="{!! asset('assets/css/style.css') !!}" media="all" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div class="container main">
            <header class="row header">
                <div class="col-md-12 text-center">
                    <h3>Teste Import Xls</h3>
                    <h4>@yield('title')</h4>
                </div>
            </header>
            <nav class="row nav">       
                <a class="col-md-6 col-sm-12 col-xs-12 text-center" href="{!! route('site.index') !!}"><b>Inicio</b></a>
                <a class="col-md-6 col-sm-12 col-xs-12 text-center" href="{!! route('site.upload') !!}"><b>Importar</b></a>
            </nav>
            <main class="container main-container">
                @yield('content')
            </main>
        </div>
        <script src="{!! asset('assets/js/jquery.min.js') !!}"></script>
        <script src="{!! asset('assets/js/jquery-mask.js') !!}"></script>
        <script src="{!! asset('assets/js/product.js') !!}"></script>
        <script src="{!! asset('assets/js/bootstrap.min.js') !!}"></script>
    </body>
</html>