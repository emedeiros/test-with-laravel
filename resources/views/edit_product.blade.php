@extends('layouts.site')

@section('title')
   {{$title}}
@endsection


@section('content')
    @if(count($product) == 0)
        <div class="row">
            <h3 class="col-md-offset-3 col-md-6 alert alert-warning">Produto n&atilde;o encontrado.</h3>
        </div>
    @else
        @if(strlen($message) > 0)
            <div class="row">
                <h3 class="col-md-offset-3 col-md-6 alert alert-{{$messageClass}}">{{$message}}</h3>
            </div>
        @endif
        {{Form::open(['url'=>$url, 'method'=>$method])}}
        <div class="form-group">
            <label for="product_external_id">ID</label>
            <label id="product_external_id"class="form-control">{{$product->product_external_id}}</label>   
        </div>
        <div class="form-group">
            <label for="name">Nome</label>
            {{Form::input("text", "name", $product->name, 
            ['class'=>'form-control', "id"=>"name", $attrForm])}}
        </div>

        <div class="form-group">
            <label for="category">Categoria</label>
            {{Form::input("text", "category",$product->category->name, ['class'=>'form-control ', $attrForm, "id"=>"category"])}}
        </div>

        <div class="form-group">
            <label for="description">Descri&ccedil;&atilde;o</label>
            {{Form::textarea("description", $product->description, ['class'=>'form-control', $attrForm, "size"=>"20x5", "id"=>"description"])}}
        </div>

        <div class="form-group">
            <label for="price">Pre&ccedil;o</label>
            {{Form::input("text", "price", number_format($product->price,2), ['class'=>'form-control', $attrForm, "id"=>"price"])}}
        </div>


        <div class="form-group">
            <label for="free_shipping">Entrega Gratu&iacute;ta</label>

            <label>Sim</label>
            <input type="radio" name="free_shipping" value="1" {{$product->free_shipping == 1 ? 'checked' : ''}}>
            <label>N&atilde;o</label>
            <input type="radio" name="free_shipping" value="0" {{$product->free_shipping == 0 ? 'checked' : ''}}>
        </div>
        <hr/>
        {{Form::submit($sendButtonName, ["class"=> "btn btn-success"])}}
        {{Form::close()}}
    @endif
@endsection