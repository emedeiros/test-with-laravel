@extends('layouts.site')

@section('title')
   Inicio
@endsection


@section('content')
    @if(count($products) == 0)
        <div class="row">
            <h3 class="col-md-offset-3 col-md-6 alert alert-warning">Nenhum item cadastrado.</h3>
        </div>
    @else
        <div class="row">
            <table class="table striped-table">
                <thead>
                    <th>ID</th>
                    <th>Categoria</th>
                    <th>Nome</th>
                    <th>Descri&ccedil;&atilde;o</th>
                    <th class="text-center">pre&ccedil;o</th>
                    <th class="text-center">Frete gratu&iacute;to</th>
                    <th class="text-center">Editar</th>
                    <th class="text-center">Apagar</th>
                </thead>
                <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td>{{$product->product_external_id}}</td>
                            <td>{{$product->category->name}}</td>
                            <td>{{$product->name}}</td>
                            <td>{{$product->description}}</td>
                            <td class="text-center">{{number_format($product->price,2)}}</td>
                            <td class="text-center">{{$product->free_shipping == 1 ? "Sim" : "N&atilde;o"}}</td>
                            <td class="text-center"> 
                                <a class="btn btn-default" 
                                   href="{{route('site.edit',$product->id)}}"
                                >Editar</a>
                            </td>
                            <td class="text-center"> 
                                <a class="btn btn-default" 
                                   href="{{route('site.delete',$product->id)}}"
                                >Apagar</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @endif
@endsection