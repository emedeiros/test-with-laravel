@extends('layouts.site')

@section('title')
   Upload de arquivo
@endsection


@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="btn-success">
                Importado arquivo com sucesso
            </div>
        </div>
    </div>
@endsection