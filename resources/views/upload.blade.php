@extends('layouts.site')

@section('title')
   Upload de arquivo
@endsection


@section('content')
    <div class="row">
    @if(strlen($message) > 0)
        <div class="row">
            <h3 class="col-md-offset-3 col-md-6 alert alert-{{$messageClass}}">{{$message}}</h3>
        </div>
    @endif
            {!! Form::open([
                'url' => route('site.upload'), 
                'type'=> 'POST', 
                'files' => true
            ]) !!}

                <div class="form-group">
                    <input type="file" name="file">
                </div>
                {!!Form::submit("Enviar", ['class' => 'btn btn-info']) !!}
            {!! Form::close() !!}
    </div>
@endsection