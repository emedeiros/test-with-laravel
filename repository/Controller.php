<?php

namespace Repository;

use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Repository\Models\RepositoryProduct;
use Repository\Models\RepositoryCategory;
use Repository\XlsReader;
use Repository\Contracts\InterfaceError;

Class Controller implements InterfaceError
{
    use DispatchesJobs;

    protected $product;
    protected $error;

    public function __construct(RepositoryProduct $product)
    {
        $this->product = $product;
    }

    /**
     * Return object products
     *
     * @return collection App\Models\Product
     */
    public function getProducts()
    {
        return $this->product->all();
    }

    /**
     * get a object product by DB IB
     *
     * @param int $id
     * @return App\Models\Product
     */
    public function getProduct($id)
    {
        return $this->product->find($id);
    }

    public function change($data, $id)
    {
        $data['category_id'] = (new RepositoryCategory)->getId($data['category']);
        if(!$this->product->changeByDbId($data, $id))
        {
            $this->error = $this->product->getError();
            return False;
        }

        return True;
    }

    /**
     * Return error catch by one of repositories class
     *
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * Delete one product based on dbId
     *
     * @param int $id
     * @return boolean
     */
    public function delete($id)
    {
        if($this->product->delete($id))
            return True;
        $this->error = $this->product->getError();
        return False;
    }
}