<?php
namespace Repository;

use PHPExcel;
use Repository\CustomException;

Class XlsReader
{
    protected $Excel;
    protected $WorkSheet;
    protected $Error;

    private $HeadCategory = 'category';
    private $HeadData = [
        'lm'                => 'A',
        'name'              => 'B',
        'free_shipping'     => 'C',
        'description'       => 'D',
        'price'             => 'E'
    ];
    /**
     * method to set Resource
     *
     * @return bool
     */
    public function setResource($file)
    {
        /*Set loader to xlsx and read only*/
        $reader = \PHPExcel_IOFactory::createReader('Excel2007');
        $reader->setReadDataOnly(True);

        try
        {
            $this->Excel = $reader->load(\storage_path('xlsx/').$file);            
        }
        catch (\Exception $e)
        {
            $this->Error = $e->getMessage();
            return False;
        }
        
        $this->WorkSheet = $this->Excel->getActiveSheet();
        return True;
    }

    /**
     * Return True when xls contains every "HEAD" column
     *
     * @return bool
     */
    public function validateDisposition()
    {
        try
        {
            $this->checkCategory();
            $this->checkDataHead();
        }
        Catch (\Exception $e)
        {
            $this->Error = $e->getMessage();
            return False;
        }

        return True;
    }

    /**
     * Return line of data from xlsx
     *
     * @return array
     */
    public function getElements()
    {
        $elements = [];
        $lastRow = $this->WorkSheet->getHighestRow();
        
        for($i = 5; $i<= $lastRow; $i++)
        {
            $elements[] = $this->getDataRow($i);
        }

        return $elements;
    }

    /**
     * Return error from some exception
     *
     * @return string
     */
    public function getError()
    {
        return $this->Error;
    }

    /**
     * return the values of one row
     *
     * @return array
     */
    private function getDataRow($line)
    {
        $row = [];
        foreach($this->HeadData as $key => $col)
        {
            $row[$key] = $this->WorkSheet->getCell($col.$line)->getValue();
        }

        return $row;
    }

    /**
     * Get category value in cell B2
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->WorkSheet->getCell('B2')->getValue();
    }

    /**
     * Return true when category head and category data is valid or throw exception
     *
     * @return bool
     */
    private function checkCategory()
    {
        $category = $this->WorkSheet->getCell('A2')->getValue();
        if($category != $this->HeadCategory)
            throw new CustomException("HEAD column category invalid");
        
        $categoryValue = $this->WorkSheet->getCell('B2')->getValue();
        if(strlen($categoryValue) == 0)
            throw new CustomException("Value of category can't be empty");

        return True;
    }

    /**
     * Return True when de disposition of data head is correct or throw exception
     *
     * @return bool
     */
    private function checkDataHead()
    {
        foreach($this->HeadData as $key => $column)
        {
            $cellValue = $this->WorkSheet->getCell($column.'4')->getValue();
            if($key != $cellValue)
                throw new CustomException("HEAD column of data is invalid. Position: ". $key);
        }
        return True;
    }
}