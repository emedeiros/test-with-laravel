<?php
namespace Repository;

use Repository\CustomException;
use Repository\Models\RepositoryProduct;
use Repository\Models\RepositoryCategory;

Class ProcessElement
{
    protected $data;
    protected $category;
    protected $error;
    protected $association = [
        'lm'            => 'product_external_id', 
        'name'          => 'name', 
        'description'   => 'description', 
        'free_shipping' => 'free_shipping', 
        'price'         => 'price' 
    ];

    public function __construct($data, $category = '')
    {
        $this->data = $data;
        $this->category = $category;
    }

    /**
     * Run every check for save one element
     *
     * @return boolean
     */
    public function check()
    {
        try
        {
            $this->checkKeys();
            $this->checkCategory();
        }
        catch (\Exception $e)
        {
            $this->error = $e->getMessage();
            return False;
        }
        return True;
    }

    /**
     * insert or update a product
     *
     * @return Boolean
     */
    public function save(RepositoryProduct $product)
    {
        $data = $this->orderToModel();

        if($product->exist($data['product_external_id']))
            return $product->change($data);

        return $product->insert($data);
    }

    /**
     * Return error catched in some Exception
     *
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * Check if every key, declared in attribute association, exist in data
     * passed in construct. Thrown Exception when someone not exist
     *
     * @return void
     */
    private function checkKeys()
    {
        $keys = array_keys($this->data);
        foreach($this->association as $key => $value)
        {
            if(!in_array($key, $keys))
                throw new CustomException("Error. Column ". $key . " not exist.");
        }

        return True;
    }

    /**
     * Check if category, passed on construct, is a valid value
     * Throw Exception if empty
     *
     * @return boolean;
     */
    private function checkCategory()
    {
        if($this->category == '')
            throw new CustomException("Category canno't be empty string");

        return True;
    }


    /**
     * Use the association array to pass to model with
     * correct names
     *
     * @return array
     */
    private function orderToModel()
    {
        $return = [];
        $category = new RepositoryCategory();
        foreach($this->data as $key => $value)
        {
            $return[$this->association[$key]] = $value;
        }

        $return["category_id"] = $category->getId($this->category);
        $return["active"] = 1;
        return $return;
    }
}