<?php

namespace Repository\Contracts;

interface InterfaceError
{
    public function getError();
}