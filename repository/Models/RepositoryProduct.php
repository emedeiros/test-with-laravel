<?php

namespace Repository\Models;

use Repository\Contracts\InterfaceError;
use Repository\CustomException;
use App\Models\Product as ProductModel;

Class RepositoryProduct implements InterfaceError
{

    protected $data;
    protected $message = '';
    protected $matchType = [
        'product_external_id'   => 'primary_key',
        'name'                  => 'string',
        'description'           => 'string',
        'free_shipping'         => 'boolean',
        'price'                 => 'float',
        'active'                => 'boolean'
    ];

    /**
     * Should return last message saved in object
     *
     * @return string
     */
    public function getError()
    {
        return $this->message;
    }

    /**
     * Should return boolean cheking if product_external_id exist in db
     *
     * @return boolean
     */
    public function exist($id)
    {
        $element = ProductModel::where("product_external_id", $id)->get()->first();
        if($element)
        {
            $this->data = $element->toArray();
            return True;
        }
        $this->message = "Product not exist";
        return False;
    }

    /**
     * Should insert a new product in db
     *
     * @return boolean
     */
    public function insert($data)
    {
        $data['active'] = 1;

        if(!$this->handlerCheckData($data))
            return False;   
    
        if($this->exist($data['product_external_id']))
        {
            $this->message = "Product exist in database.";
            return False;
        }

        $this->data = ProductModel::create($data);
        return True;
    }

    /**
     * Should change a existing product in db
     *
     * @return boolean
     */
    public function change($data)
    {
        $data['active'] = 1;
        if(!$this->handlerCheckData($data))
            return False;

        if(!$this->exist($data['product_external_id']))
            return False;
        
        $element = ProductModel::where("product_external_id", $data['product_external_id'])
                         ->get()
                         ->first();
        $element->update($data);
        $this->data = $element;
        return True;
    }

    /**
     * Change one product based on dbId
     *
     * @param array $data
     * @param int $id
     * @return boolean
     */
    public function changeByDbId($data, $id)
    {
        $product = ProductModel::find($id);
        if(!$product)
            return False;

        $data['product_external_id'] = $product->product_external_id;

        return $this->change($data);
    }

    /**
     * Return collection with all products
     *
     * @return collection App\Models\Product
     */
    public function all()
    {
        return ProductModel::all();
    }

    /**
     * Find one element based on dbId
     *
     * @return App\Models\Product
     */
    public function find($id)
    {
        return ProductModel::find($id);
    }

    /**
     * return one stance of model Product
     *
     * @return App\Models\Product
     */
    public function getInstance()
    {
        return new ProductModel;
    }

    /**
     * delete one product
     *
     * @return boolean
     */
    public function delete($id)
    {
        $product = ProductModel::find($id+0);
        if(!$product)
        {
            $this->error = "Product not found";
            return False;
        }
        $product->delete();
        return True;
    }

    /**
     * Method who catch the exception from checkData and return boolen to other methods
     *
     * @return boolean
     */
    private function handlerCheckData($data)
    {
        Try
        {
            $this->checkData($data);   
        }
        catch(\Exception $e)
        {
            $this->message = $e->getMessage();
            return False;
        }
        return True;
    }

    /**
     * Check if every data match with type expected type.
     * thrown an Exception if someone not match.
     *
     * @return boolean
     */
    private function checkData($data)
    {
        foreach($this->matchType as $key => $value)
        {
            if(!isset($data[$key]))
                throw new CustomException("Column ". $key." not exist.");

            if(!$this->typeMatch($value, $data[$key]))
                throw new CustomException("Column ". $key . " missmatch type.");
        }
        return True;
    }

    /**
     * Return a simple validation of elements type
     *
     * @return boolean
     */
    private function typeMatch($expected, $element)
    {
        switch($expected)
        {
            case 'primary_key':
                return is_numeric($element) && $element > 0;
            case 'integer':
            case 'float':
                return is_numeric($element);
                break;
            case 'boolean':
                return in_array($element, [0,1]);
                break;
            default:
                return strlen($element) > 0;
                break;
        }
    }
}