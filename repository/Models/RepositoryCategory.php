<?php

namespace Repository\Models;

use App\Models\Category as CategoryModel;
use Repository\CustomException;
use Repository\Contracts\InterfaceError;


Class RepositoryCategory implements InterfaceError
{
    public $data = [];
    protected $error;

    /**
     * Verify if category exist
     *
     * @return boolean
     */
    public function exist($element)
    {
        $exist = CategoryModel::where("name", $element)->get()->first();
        if($exist)
        {
            $this->data = $exist->toArray();
            return True;
        }

        return False;
    }

    /**
     * Insert a New category when don't exist
     *
     * @return boolean
     */
    public function insert($element)
    {
        if($this->exist($element))
            return False;

        try
        {
            $this->checkData($element);
        }
        catch(\Exception $e)
        {
            $this->error = $e->getMessage();
            return False;
        }
        $new = new CategoryModel();
        $new->name = $element;
        $retorno = $new->save();
        $this->data = $new->toArray();
        return $retorno;
    }

    /**
     * Try to insert a new element and if exist return id
     *
     * @return integer
     */
    public function getId($element)
    {
        $this->insert($element);
        return $this->data['id'];
    }

    /**
     * return a message of error
     *
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }
    

    /**
     * Verify if data to category is ok and thrown exception if not
     *
     * @return boolean
     */
    private function checkData($value)
    {
        if(strlen(trim($value)) === 0)
            throw new CustomException("Category name cannot be empty value");

        return True;
    }

}