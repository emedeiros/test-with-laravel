<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['as'=>'site.', 'prefix'=>'/'], function(){
    route::get('',            ['as'=>'index', 'uses'=>'HomeController@index']);
    route::get('importar',    ['as' =>'upload', 'uses'=>'HomeController@upload']);
    route::post('importar',   ['uses'=>'HomeController@newFile']);
    route::get('editar/{id}', ['as'=>'edit', 'uses'=>'HomeController@edit']);
    route::put('editar/{id}', ['as'=>'edit', 'uses'=>'HomeController@update']);
    route::get('apagar/{id}', ['as'=>'delete', 'uses'=>'HomeController@delete']);
    route::delete('apagar/{id}', ['as'=>'delete', 'uses'=>'HomeController@deleteEl']);

});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
