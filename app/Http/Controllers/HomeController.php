<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Jobs\ProcessXlsxJob;
use Repository\Controller as RepositoryController;

class HomeController extends Controller
{

    protected $repo;
    protected $message = '';
    protected $messageClass;

    public function __construct(RepositoryController $repo)
    {
        $this->repo = $repo;
    }

    public function index()
    {
        return view('home', ['products' => $this->repo->getProducts()]);
    }

    public function upload()
    {
        return view('upload', ['message'=>$this->message, 'messageClass'=>$this->messageClass]);
    }

    public function newFile(Request $request)
    {
        if($request->hasFile('file'))
        {
            $name = $request->file("file")->getClientOriginalName();
            $request->file('file')
                    ->move(
                        storage_path("xlsx"), 
                        $name
                    );
            $processar = new ProcessXlsxJob($name); 
            $this->dispatch($processar);
            $this->message = "Arquivo enviado com sucesso";
            $this->messageClass = "success";
            return $this->upload();
        }
        $this->messageClass = "warning";
        $this->message = "Favor escolher um arquivo";
        return $this->upload();
    }
    
    public function edit($id)
    {
        return view(
            'edit_product', 
            [
                'product' => $this->repo->getProduct($id),
                'message' => $this->message,
                'messageClass'   => $this->messageClass,
                'attrForm' => '',
                'sendButtonName' => "Atualizar",
                'url' => route('site.edit', $id),
                'method' => 'put',
                'title' => "Editar Produto"
            ]
        );
    }

    public function update(Request $request, $id)
    {
        $this->message = "Atualizado com sucesso";
        $this->messageClass="success";
        $data = $request->input();
        if(!$this->repo->change($data, $id))
        {
            $this->message = $this->repo->getError();
            $this->messageClass = "danger";
        }

        return $this->edit($id);
    }

    public function delete($id)
    {
        return view(
            'edit_product', 
            [
                'product' => $this->repo->getProduct($id),
                'message' => $this->message,
                'messageClass'   => $this->messageClass,
                'attrForm' => 'disabled',
                'sendButtonName' => "Excluir",
                'url' => route('site.delete', $id),
                'method' => 'delete',
                'title' => "Excluir Produto"
            ]
        );
    }

    public function deleteEl($id)
    {
        if($this->repo->delete($id))
            return redirect('/');

        $this->message = $this->repo->getError();
        $this->messageClass = "danger";
        return $this->delete($id);
    }
}
