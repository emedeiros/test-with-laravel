<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Repository\XlsReader;
use Repository\ProcessElement;
use Repository\Models\RepositoryProduct;
class ProcessXlsxJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $file;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($fileName)
    {
        $this->file = $fileName;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $xlsReader = new XlsReader();
        $xlsReader->setResource($this->file);
        $elements = [];
        if($xlsReader->validateDisposition())
            $elements = $xlsReader->getElements();

        foreach($elements as $element)
        {
            $processar = new ProcessElement($element, $xlsReader->getCategory());
            if($processar->check())
                $processar->save(new RepositoryProduct);
        }
    }
}
