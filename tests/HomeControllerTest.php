<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Repository\Models\RepositoryProduct;
use Repository\Models\RepositoryCategory;

class HomeControllerTest extends TestCase
{
    use DatabaseMigrations;

    protected $product = [
        'product_external_id'   => '1001', 
        'name'                  => 'Broca', 
        'description'           => 'Broca dura', 
        'free_shipping'         => 0, 
        'price'                 => '199.99', 
        'active'                => 1,
        'category_id'           => 1
    ];

    /**
     * Home should show zero items
     *
     * @return void
     */
    public function testHomeShouldHaveNoItem()
    {
        $this->visit('/')
             ->see('Inicio')
             ->see('Nenhum item cadastrado.');
    }

    /**
     * Home Should show one item
     *
     * @return void
     */
    public function testHomeShouldHaveOneItem()
    {
        $this->initElement();
        $this->visit('/')
             ->see('Inicio')
             ->see($this->product['name']);
    }

    public function testHomeClickImport()
    {
        $this->visit('/')
             ->see('Inicio')
             ->click('Importar')
             ->see('Upload');
    }

    /**
     * Home should show one item and click in edit
     *
     * @return void
     */     
    public function testHomeClickInEdit()
    {
        $this->initElement();
        $this->visit('/')
             ->see('Inicio')
             ->see($this->product['name'])
             ->click("Editar");
    }

    /**
     * Home should show one item and click in edit
     *
     * @return void
     */     
    public function testHomeClickInDelete()
    {
        $this->initElement();
        $this->visit('/')
             ->see('Inicio')
             ->see($this->product['name'])
             ->click("Apagar")
             ->see("Excluir Produto");
    }

    /**
     * Home should show one item and click in edit
     *
     * @return void
     */     
    public function testHomeClickInDeleteAndReturnToHomeAfter()
    {
        $this->initElement();
        $this->visit('/')
             ->see('Inicio')
             ->see($this->product['name'])
             ->click("Apagar")
             ->see("Excluir Produto")
             ->press("Excluir")
             ->see("Nenhum item cadastrado");
    }

    /**
     * Home should show one item and click in edit
     *
     * @return void
     */     
    public function testHomeClickInDeleteAndReturnWarningMessageOfNotFound()
    {
        $this->initElement();
        $this->visit('/')
             ->see('Inicio')
             ->see($this->product['name'])
             ->click("Apagar")
             ->see("Excluir Produto");
        $product = new RepositoryProduct();
        $product->delete(1);
        $this->press("Excluir")
             ->see("Produto não encontrado");
    }

    /**
     * edit should show message of warning about product that not exist
     *
     *
     */
    public function testEditShouldShowWarningProductNotExist()
    {
        $this->initElement();
        $this->visit(route('site.edit',0))
             ->see('Produto não encontrado.');
    }

    public function testControllerNewFileShouldShowMessageOfSuccess()
    {
        $this->visit(route('site.upload'))
             ->attach(storage_path('xlsx').'/test/phpunit.xlsx','file')
             ->press('Enviar')
             ->see("Inicio")
             ->see("Arquivo enviado com sucesso");
    }

    public function testControllerNewFileShouldRedirectToFirstPageWithElements()
    {
        $this->visit(route('site.upload'))
             ->attach(
                storage_path('xlsx').'/test/products_teste_webdev_leroy.xlsx',
                'file'
             )
             ->press('Enviar')
             ->see('Arquivo enviado com sucesso')
             ->visit('/')
             ->see("Inicio")
             ->see("ID")
             ->see("Categoria")
             ->see("Nome")
             ->see("Descrição")
             ->see("Editar")
             ->see("Frete gratuíto");
    }

    public function testControllerWithoutFileShouldReturnUploadView()
    {
        $this->visit(route('site.upload'))
             ->press('Enviar')
             ->see("Upload");
    }

    public function testUpdateElementShouldShouldShowSuccessMessage()
    {
        $this->initElement();
        $this->visit(route('site.edit', 1))
             ->see("Editar Produto")
             ->type("Broca Mole", "name")
             ->press("Atualizar")
             ->see("alert-success");
    }

    public function testUpdateElementShouldShouldShowDangerMessageAndNotUpdateElement()
    {
        $this->initElement();
        $this->visit(route('site.edit', 1))
             ->see("Editar Produto")
             ->type("", "name")
             ->press("Atualizar")
             ->see("alert-danger");
    }

    private function initElement()
    {
        $category = new RepositoryCategory();
        $product = new RepositoryProduct();
        $this->product['category_id'] = $category->getId("Ferramentas");
        $product->insert($this->product);
    }
}
