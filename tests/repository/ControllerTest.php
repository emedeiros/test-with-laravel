<?php

use Repository\Models\RepositoryCategory;
use Repository\Models\RepositoryProduct;
use Repository\Controller;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Mockery as m;

Class ControllerTest Extends TestCase
{
    use DatabaseMigrations;

    protected $product;

    public function setUp()
    {
        parent::setUp();
        $this->controller = new Controller(new RepositoryProduct);
        $this->InsertData();
    }

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    public function testGetProductsShouldReturnArrayWithTenElements()
    {
        $this->assertEquals(10, count($this->controller->getProducts()));
    }

    public function testGetProductShouldReturnOneProduct()
    {
        $this->assertEquals(1, count($this->controller->getProduct(1)));
    }

    public function testChangeProductShouldReturnFalseWhenErrorOccurDuringUpdate()
    {
        $product = m::mock(RepositoryProduct::class);
        $array = ['category'=>"category", "category_id"=>2];
        $id = 0;

        $product->shouldReceive("changeByDbId")
                ->with($array, $id)
                ->once()
                ->andReturn(False);

        $product->shouldReceive("getError")
                ->once()
                ->andReturn("Erro");

        $controller = new Controller($product);
        $this->assertFalse($controller->change($array, $id));
        $this->assertStringStartsWith("Erro", $controller->getError());
    }

    public function testChangeProductShouldReturnTrue()
    {
        $product = m::mock(RepositoryProduct::class);
        $array = ['category'=>"category", "category_id"=>2];
        $id = 1;

        $product->shouldReceive("changeByDbId")
                ->with($array, $id)
                ->once()
                ->andReturn(True);

        $product->shouldNotReceive("getError");

        $controller = new Controller($product);
        $this->assertTrue($controller->change($array, $id));
    }

    private function insertData()
    {
        (new RepositoryCategory)->insert("Ferramentas");
        
        $product = new RepositoryProduct();
        $data = [ 
            'name'                  => 'Broca', 
            'description'           => 'Broca dura', 
            'free_shipping'         => 0, 
            'price'                 => '199.99', 
            'active'                => 1,
            'category'              => "Broca",
            "category_id"           => 1
        ];
        for($i = 1; $i <= 10; $i++)
        {
            $data['product_external_id'] = $i;
            $product->insert($data);
        }
    }
}