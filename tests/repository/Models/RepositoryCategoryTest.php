<?php

use Repository\Models\RepositoryCategory as Category;
use Illuminate\Foundation\Testing\DatabaseMigrations;

Class RepositoryCategoryTest extends TestCase
{
    use DatabaseMigrations;
    
    /**
     * Verify if exist return false when pass one category that not exist
     *
     * @return void
     */
    public function testExistShouldReturnFalseWhenCategoryNotExist()
    {
        $category = new Category();
        $this->assertFalse($category->exist("Test"));
    }

    /**
     * Verify if insert of element return true when he not exist
     *
     * @return void
     */
    public function testInsertShouldReturnTrueWhenNotExist()
    {
        $category = new Category();
        $this->assertTrue($category->insert("test_Ferramentas"));
    }

    /**
     * Verify if insert return false when a empty value is passed
     *
     * @return void
     */
    public function testInsertShouldReturnFalseWhenValueEmpty()
    {
        $category = new Category();
        $this->assertFalse($category->insert(""));
        $this->assertStringStartsWith(
            "Category name cannot be empty value", 
            $category->getError()
        );
    }

    /**
     * Verify if insert return false when the element exist is passed
     *
     * @return void
     */
    public function testInsertShouldReturnFalseWhenExist()
    {
        $category = new Category();
        $category->insert("test_Ferramentas");
        $this->assertFalse($category->insert("test_Ferramentas"));
        $this->assertEquals(1, $category->data["id"]);
    }

    /**
     * Should return id of element.
     *
     * @return void
     */
    public function testGetIdShouldReturnOne()
    {
        $category = new Category();
        $category->insert("test_ferramentas");
        $this->assertEquals(2, $category->getId("Nova Ferramenta")); 
    }
}