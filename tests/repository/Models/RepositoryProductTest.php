<?php

use Repository\Models\RepositoryProduct;
use Repository\Models\RepositoryCategory;
use Illuminate\Foundation\Testing\DatabaseMigrations;

Class RepositoryProductTest extends TestCase
{
    use DatabaseMigrations;

    protected $product;
    protected $category;
    protected $data = [
        'product_external_id'   => '1001', 
        'name'                  => 'Broca', 
        'description'           => 'Broca dura', 
        'free_shipping'         => 0, 
        'price'                 => '199.99', 
        'active'                => 1,
        'category_id'           => 1
    ];

    public function setUp()
    {
        parent::setUp();
        $this->initElements();
    }

    /**
     * exist should return false when element not exist in DB.
     *
     * @return void
     */
    public function testExistShouldReturnFalseWhenElementNotExist()
    {
        $this->assertFalse($this->product->exist(1));
    }

    /**
     * insert should return false when price not float.
     *
     * @return void
     */
    public function testInsertShouldReturnFalseWhenPriceIsString()
    {
        $data = $this->data;
        $data['price'] = "string";
        $this->assertFalse($this->product->insert($data));
        $this->assertStringStartsWith(
            "Column price missmatch type.",
            $this->product->getError()
        );
    }

    /**
     * insert should return false when price key of data passed not exist
     *
     * @return void
     */
    public function testInsertShouldReturnFalseWhenPriceKeyNotExist()
    {
        $data = $this->data;
        unset($data['price']);
        $this->assertFalse($this->product->insert($data));
        $this->assertStringStartsWith(
            "Column price not exist.",
            $this->product->getError()
        );
    }

    /**
     * insert should return false when product_external_id is empty or zero
     * and set an error message
     *
     * @return void
     */
    public function testCheckShouldReturnFalseWhenValueIsZeroOrEmpty()
    {
        foreach([0, ""] as $checkElement)
        {
            $processor = NULL;
            $data = $this->data;
            $data['product_external_id'] = $checkElement;
            $this->assertFalse($this->product->insert($data));
            $this->assertStringStartsWith(
                "Column product_external_id missmatch type.",
                $this->product->getError()
            );
        }        
    }

    /**
     * exist should return True when element in db
     *
     * @return void
     */
    public function testExistShouldReturnTrueWhenElementExist()
    {
        $this->product->insert($this->data);
        $this->assertTrue($this->product->exist($this->data['product_external_id']));
    }

    /**
     * insert should return truen when element not exist and is inserted
     *
     * @return void
     */
    public function testInsertShouldReturnTrueWhenNotExist()
    {
        $this->assertTrue($this->product->insert($this->data));
    }

    /**
     * Insert should return false when element exist in db
     * and an error message should be set
     *
     */
    public function testInsertShouldReturnFalseWhenExist()
    {
        $this->product->insert($this->data);
        $this->assertFalse($this->product->insert($this->data));
        $this->assertStringStartsWith(
            'Product exist in database.',
            $this->product->getError()
        );
    }

    /**
     * Change should return true when element exist in db
     * Theres a second validation that check if product was updated
     *
     * @return void
     */
    public function testChangeShouldReturnTrueWhenElementExist()
    {
        $this->product->insert($this->data);
        $this->data["name"] = "Broca mole";
        $this->assertTrue($this->product->change($this->data));
        $updated = $this->product
                        ->getInstance()
                        ->where(
                            "product_external_id", 
                            $this->data['product_external_id']
                        )
                        ->first()->toArray();
        $this->assertStringStartsWith("Broca mole", $updated['name']);
    }


    /**
     * Change should return false when element exist in db
     * but the new value passed is invalid
     * And should Validate the message
     *
     * @return void
     */
    public function testChangeShouldReturnFalseWhenElementExistButNewValueIsInvalid()
    {
        $this->product->insert($this->data);
        $data = $this->data;
        $data["name"] = "";
        $this->assertFalse($this->product->change($data));
        $this->assertStringStartsWith(
            "Column name missmatch type.",
            $this->product->getError()
        );
    }
    /**
     * Change should return False when element not exist in db
     *
     * @return void
     */
    public function testChangeShouldReturnFalseWhenElementNotExist()
    {
        $this->assertFalse($this->product->change($this->data));
        $this->assertStringStartsWith(
            'Product not exist',
            $this->product->getError()
        );
    }

    /**
     * changeById should return False when element not exist in db
     *
     * @return void
     */
    public function testChangeByIdShouldReturnFalseWhenProductNotExist()
    {
        $this->initElements();
        $this->assertFalse($this->product->changeByDbId($this->data, 3));
    }


    /**
     * changeById should return False when Product exist in db but name has a invalid
     * value(empty)
     *
     * @return void
     */
    public function testChangeByIdShouldReturnFalseWhenProductExistButHaveMissData()
    {
        $this->initElements();
        $this->product->insert($this->data);
        $data = $this->data;
        $data['name'] = "";
        $this->assertFalse($this->product->changeByDbId($data, 1));
        $this->assertStringStartsWith(
            "Column name missmatch type", 
            $this->product->getError()
        );
    }

    /**
     * changeById should return true when product is updated
     *
     * @return void
     */
    public function testChangeByIdShouldReturnTrueWhenProductExist()
    {
        $this->initElements();
        $this->product->insert($this->data);
        $this->assertTrue($this->product->changeByDbId($this->data, 1)); 
    }

    private function initElements()
    {
        $this->product = new RepositoryProduct();
        $this->category = new RepositoryCategory();
        $this->category->insert("Ferramentas");
    }
}