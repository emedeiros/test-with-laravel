<?php

use Repository\XlsReader;

Class XlsReaderTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->XlsReader = new XlsReader();
    }

    /**
     * function that expect false when resource does not exist
     *
     * @return void
     */
    public function testSetResourceShouldReturnFalseWhenResourceNotExist()
    {
        $this->assertFalse($this->XlsReader->setResource('test/i_am_not_exist'));
    }

    /**
     * function that expect a true assertion when resource exist
     *
     * @return void
     */
    public function testSetResourceShouldReturnTrueWhenResourceExist()
    {
        $this->assertTrue($this->XlsReader->setResource('test/phpunit.xlsx'));
    }

    /**
     * function that expect a false assertion when resource exist but the time is not xlsx
     *
     * @return void
     */
    public function testSetResourceShouldReturnFalseWhenResourceExistButTypeWrong()
    {
        $this->assertFalse($this->XlsReader->setResource('test/text.txt'));
    }

    /**
     * function that expect False for a invalid xlsx disposition
     *
     * @return void
     */
    public function testValidateDispositionShouldReturnFalseWhenXlsxCategoryIsInvalid()
    {
        $this->assertTrue($this->XlsReader->setResource('test/invalid_category.xlsx'));
        $this->assertFalse($this->XlsReader->validateDisposition());
        $this->assertEquals('HEAD column category invalid', $this->XlsReader->getError());
    }

    /**
     * function that expect false because data head is invalid
     *
     * @return void
     */
    public function testValidateDispositionShouldReturnFalseWhenDataHeadIsInvalid()
    {
        $this->assertTrue($this->XlsReader->setResource('test/invalid_data_head.xlsx'));
        $this->assertFalse($this->XlsReader->validateDisposition());
        $this->assertStringStartsWith(
            'HEAD column of data is invalid. Position: ', 
            $this->XlsReader->getError()
        );
    }

    /**
     * function that expect true when every head is valid
     *
     * @return void
     */
    public function testValidateDispositionShouldReturnTrue()
    {
        $this->assertTrue(
            $this->XlsReader->setResource('test/products_teste_webdev_leroy.xlsx')
        );
        $this->assertTrue($this->XlsReader->validateDisposition());
        $this->assertEquals('',$this->XlsReader->getError());
    }

    /**
     * getElement should return an array with six elements inserted 
     * with a test excel
     *
     * @return void
     */
    public function testgetElementsShouldReturnAnArrayWithSixElements()
    {
        $this->assertTrue(
            $this->XlsReader->setResource('test/products_teste_webdev_leroy.xlsx')
        );
        $elements = $this->XlsReader->getElements();
        $this->assertEquals(6, count($elements));
        $this->assertEquals(5, count($elements[0]));
    }

    /**
     * getCategory should return a string of test excel
     *
     * @return void
     */
    public function testGetCategoryShouldReturnString()
    {
        $this->XlsReader->setResource('test/products_teste_webdev_leroy.xlsx');
        $this->assertEquals('Ferramentas', $this->XlsReader->getCategory());
    }
}