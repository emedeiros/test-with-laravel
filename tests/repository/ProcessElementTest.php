<?php

use Repository\ProcessElement;
use Repository\Models\RepositoryProduct;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Mockery as m;


Class ProcessElementTest Extends TestCase
{
    use DatabaseMigrations;

    protected $data = [
        'lm'            => '10012',
        'name'          => 'Broca',
        'free_shipping' => '0',
        'description'   => 'Broca de madeira',
        'price'         => '200.01'
    ];

    public function teadDown()
    {
        m::close();
        parent::tearDown();
    }

    /**
     * check should return False when columns of product exist but category is empty.
     *
     * @return void
     */
    public function testCheckShoulReturnFalseWhenCategoryEmpty()
    {
        $processor = new ProcessElement($this->data, "");
        $this->assertFalse($processor->check());
        $this->assertStringStartsWith(
            "Category canno't be empty string",
            $processor->getError()
        );
    }


    /**
     * check should return False when column free_shipping to model not exist.
     *
     * @return void
     */
    public function testCheckShouldReturnFalseWhenFreeShippingNotExists()
    {
        $processor = new ProcessElement(
            [
                'lm'            => '',
                'description'   => '',
                'name'          => '',
                'price'         => ''
            ], 
            "Ferramentas"
        );
        $this->assertFalse($processor->check());
        $this->assertStringStartsWith(
            'Error. Column free_shipping not exist.', 
            $processor->getError()
        );
    }

    /**
     * check should return True when every column for a product exist.
     *
     * @return void
     */
    public function testCheckShoulReturnTrueWhenEveryCheckPass()
    {
        $processor = new ProcessElement($this->data, "Ferramentas");
        $this->assertTrue($processor->check());
    }

    /**
     * save should return true when product in inserted on db
     *
     * @return void
     */
    public function testSaveShouldInsertProductInDb()
    {
        $process = new ProcessElement($this->data, "Ferramentas");
        $this->assertTrue($process->save(new RepositoryProduct));
    }

    /**
     * Save should upate a product in db and return true
     * second validation check if product name was updated in db
     *
     * @return void
     */
    public function testSaveShouldUpdateProductInDb()
    {

        $process = new ProcessElement($this->data, "Ferramentas");
        $process->save(new RepositoryProduct);
        
        $dataUpdate = $this->data;
        $dataUpdate["name"] = "Broca mole";
        $dataUpdate["price"] = 12;        
        $process = new ProcessElement($dataUpdate, "Ferramentas");

        $this->assertTrue($process->save(new RepositoryProduct));
        $element = (new RepositoryProduct)->getInstance()
                          ->where("product_external_id", $this->data["lm"])
                          ->get()
                          ->first()
                          ->toArray();
        $this->assertEquals("Broca mole", $element["name"]);
    }

    /**
     * Mock should validate the correct call of product methods
     * when ProcessElement save is called and should call insert method
     *
     * @return void
     */
    public function testSaveShouldSimulateNewItemInsertion()
    {
        $product = m::mock(RepositoryProduct::class);
        $product->shouldReceive("exist")
                ->once()
                ->andReturn(False);
        
        $product->shouldReceive("insert")
                ->once()
                ->andReturn(True);

        $product->shouldNotReceive("change");

        $process = new ProcessElement($this->data, "Ferramentas");

        $this->assertTrue($process->save($product));
    }

    /**
     * Mock should validate the correct call of product methods
     * when ProcessElement save is called and should call change method
     *
     * @return void
     */
    public function testSaveShouldSimulateItemUpdate()
    {
        $product = m::mock(RepositoryProduct::class);
        $product->shouldReceive("exist")
                ->once()
                ->andReturn(True);

        $product->shouldReceive("change")
                ->once()
                ->andReturn(True);

        $product->shouldNotReceive("insert");

        $process = new ProcessElement($this->data, "Ferramentas");

        $this->assertTrue($process->save($product));
    }
}