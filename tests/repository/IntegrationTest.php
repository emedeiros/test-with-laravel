<?php

use Repository\XlsReader;
use Repository\ProcessElement;
use Repository\Models\RepositoryProduct;
use Illuminate\Foundation\Testing\DatabaseMigrations;

Class IntegrationTest extends TestCase
{
    use DatabaseMigrations;

    public function testShouldInsertSixProductsInDb()
    {
        $reader = new XlsReader();
        $reader->setResource('test/products_teste_webdev_leroy.xlsx');
        $this->assertTrue($reader->validateDisposition());
        $elements = $reader->getElements();
        $category = $reader->getCategory();
        foreach($elements as $element)
        {
            $new = new ProcessElement($element, $category);
            if($new->check())
                $new->save(new RepositoryProduct);
        }
        $this->assertEquals(6, (new RepositoryProduct)->all()->count());
    }

    public function testShouldInserThreeProductsInDbWhenOtherTwoHaveBadData()
    {
        $reader = new XlsReader();
        $reader->setResource('test/products_teste_webdev_leroy_v2.xlsx');
        $this->assertTrue($reader->validateDisposition());
        $elements = $reader->getElements();
        $category = $reader->getCategory();
        foreach($elements as $element)
        {
            $new = new ProcessElement($element, $category);
            if($new->check())
                $new->save(new RepositoryProduct);
        }
        $this->assertEquals(3, (new RepositoryProduct)->all()->count());
    }
}
