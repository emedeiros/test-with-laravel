<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Products', function(Blueprint $table) {
            $table->increments("id");
            $table->integer("product_external_id")->unsigned();
            $table->string("name", 200);
            $table->string("description", 1000);
            $table->boolean("free_shipping");
            $table->integer("category_id")->unsgined();
            $table->float("price");
            $table->boolean("active");
            $table->timestamps();

            $table->foreign('category_id')
                  ->references('id')
                  ->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("Products");
    }
}
